require 'prime'

class Jamcoin
  def initialize(string_jamcoin)
    @string_jamcoin = string_jamcoin
    @nontrivial_divisors = []
  end

  def legitimate?
    2.upto(10) do |base|
      base_interpretation = @string_jamcoin.to_i(base)
      nontrivial_divisor = first_divisor(base_interpretation)

      return false unless nontrivial_divisor > 0
      @nontrivial_divisors << nontrivial_divisor
    end

    true
  end

  def to_s
    "#{@string_jamcoin} #{@nontrivial_divisors.join(' ')}"
  end

  private

  def first_divisor(number)
    Prime.each do |prime|
      return prime if number % prime == 0

      # Shady optimization trick, discard jamcoin if too much hassle to find divisor
      return -1    if prime > 42

      # "Correct" way would be this
      # return -1    if prime > Math.sqrt(number).floor
    end
  end
end
