require './jamcoin'

class JamcoinBank
  def initialize(jamcoin_size)
    @jamcoin_size = jamcoin_size
  end

  def create_jamcoins(number_of_jamcoins)
    jamcoins = []

    0.upto(max_number_of_jamcoins - 1) do |jamcoin_stem|
      jamcoin = Jamcoin.new("1#{stem_to_s(jamcoin_stem)}1")

      jamcoins << jamcoin if jamcoin.legitimate?

      return jamcoins if jamcoins.length >= number_of_jamcoins
    end

    raise 'Not enough jamcoins! Try increasing the magic number
           used for shady optimization.'
  end

  private

  def max_number_of_jamcoins
    @max_number_of_jamcoins ||= 2**(@jamcoin_size - 2)
  end

  def stem_to_s(number)
    stem = number.to_s(2)
    stem.prepend('0' * (@jamcoin_size - 2 - stem.size))
  end
end
