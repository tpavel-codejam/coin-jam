require './jamcoin_bank'

t = gets.to_i

1.upto(t) do |i|
  values = gets.split(' ').map(&:to_i)
  n = values.first
  j = values.last

  if j > 2**(n - 2)
    raise "The number of required jamcoins is too damn high!
           It's impossible to make more than 2^(n-2)."
  end

  jamcoin_bank = JamcoinBank.new(n)

  puts "Case ##{i}:"
  jamcoin_bank.create_jamcoins(j).each do |jamcoin|
    puts jamcoin
  end
end
